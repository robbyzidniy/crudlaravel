<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('adminlte.items.index');
});


Route::get('/master', function() {
    return view('adminlte.master');
});


Route::get('/data-tables', function() {
    return view('adminlte.items.data');
});


Route::get('/pertanyaan/create', 'App\Http\Controllers\PertanyaanController@create');


Route::post('/pertanyaan', 'App\Http\Controllers\PertanyaanController@store');


Route::get('/pertanyaan', 'App\Http\Controllers\PertanyaanController@index');


Route::get('pertanyaan/{id}', 'App\Http\Controllers\PertanyaanController@show');


Route::get('/pertanyaan/{id}/edit', 'App\Http\Controllers\PertanyaanController@edit');

Route::put('pertanyaan/{id}', 'App\Http\Controllers\PertanyaanController@update');


Route::delete('pertanyaan/{id}', 'App\Http\Controllers\PertanyaanController@destroy');










