<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    public function create(){
        return view('adminlte.items.pertanyaan.create');
    }

    public function store(Request $request){
        $request->validate([
            "title" => "required",
            "body" => "required"
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["title"],
            "isi" => $request["body"]
        ]);
        
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');
    }

    public function index(){
        $post = DB::table('pertanyaan')->get();
        return view('adminlte.items.pertanyaan.index', compact('post'));
    }

    public function show($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view ('adminlte.items.pertanyaan.show', compact('post'));
    }

    public function edit($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('adminlte.items.pertanyaan.edit', compact('post'));
    }

    public function update($id, Request $request){
        $request->validate([
            "title" => "required",
            "body" => "required"
        ]);

        $query = DB::table('pertanyaan')
        ->where('id', $id )
        ->update([
            'judul' => $request['title'],
            'isi' => $request['body']
        ]);

        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Diupdate!');

    }

    public function destroy($id){
        $query = DB::table('pertanyaan')
        ->where('id', $id)
        ->delete();

        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dihapus!');
    }
}
