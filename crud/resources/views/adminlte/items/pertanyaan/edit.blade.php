@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header p-3">
        <div>
            <div class="card-header mb-2">
                <h2 class="card-title">Edit Post {{$post->id}}</h2>
            </div>
            <form action="/pertanyaan/{{$post->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" value="{{old('title',$post->judul)}}" id="title" placeholder="Masukkan Title">
                    @error('title')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">body</label>
                    <input type="text" class="form-control" name="body"  value="{{ old('body',$post->isi) }}"  id="body" placeholder="Masukkan Body">
                    @error('body')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>
</div>
@endsection

