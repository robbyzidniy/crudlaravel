@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h2>Tambah Data</h2>
        <form action="/pertanyaan" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" class="form-control" value="{{ old('title', '')}}" name="title" id="title" placeholder="Masukkan Judul">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Isi</label>
                <input type="text" class="form-control" value="{{ old('body', '')}}" name="body" id="body" placeholder="Masukkan Pertanyaan">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
</div>
@endsection