@extends('adminlte.master')


@section('content')
<div class="card">
    <div class="card-header p-3">
        <h2>Show Post {{$post->id}}</h2>
        <h4>{{$post->judul}}</h4>
        <p>{{$post->isi}}</p>
    </div>
</div>
@endsection